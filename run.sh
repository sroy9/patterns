#!/bin/sh
HEAP=20g

time java -ea -cp classes/:lib/* -Xmx$HEAP -XX:MaxPermSize=512m $@

