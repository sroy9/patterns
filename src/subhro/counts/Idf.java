package subhro.counts;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset.RelationAndMentions;

public class Idf {

	public static List<String> getTokensFromFeature(String feature) {
		List<String> toks = new ArrayList<String>();
		String segments[] = feature.split("\\|");
		int midIndex = -1;
		if(segments.length ==  4) {
			midIndex = 2;
		} else if(segments.length ==  6) {
			midIndex = 3;
		}
		List<String> bigrams = new ArrayList<>();
		if(midIndex>=0 && midIndex < segments.length) {
			String strArr[] = segments[midIndex].split(" ");
			for(int i=0; i<strArr.length-1; ++i) {
				bigrams.add(strArr[i]+"_"+strArr[i+1]);
			}
		}
		if(bigrams.size()==0) {
			toks.add(feature);
			return toks;
		}
		for(String bigram : bigrams) {
			String str = "";
			for(int i=0; i<segments.length; ++i) {
				if(i==midIndex) {
					str += bigram + "|";
				} else {
					str += segments[i] + "|";
				}
			}
			toks.add(str.substring(0, str.length()-1));
		}
		return toks;
	}
	
	public static Pair<List<RelationAndMentions>, List<RelationAndMentions>> getMingWeiSplit(
			List<RelationAndMentions> train, List<RelationAndMentions> test, String relation) {
		int maxNeg = 1000;
		List<RelationAndMentions> newTrain = new ArrayList<RelationAndMentions>();
//		List<RelationAndMentions> newTest = new ArrayList<RelationAndMentions>();
		int pos = 0, neg = 0;
		for(RelationAndMentions relMention : train) {
			if(relMention.posLabels.contains(relation)) {
				newTrain.add(relMention);
				pos++;
			}
			if(!relMention.posLabels.contains(relation) && neg < maxNeg) {
				newTrain.add(relMention);
				neg++;
			}
		}
		if(neg < maxNeg) System.out.println("Less neg than Maxneg");
//		pos = 0; neg = 0;
//		for(RelationAndMentions relMention : test) {
//			if(relMention.posLabels.contains(relation) && pos < maxPos) {
//				newTest.add(relMention);
//				pos++;
//			}
//			if(!relMention.posLabels.contains(relation) && neg < maxNeg) {
//				newTest.add(relMention);
//				neg++;
//			}
//			if(pos >= maxPos && neg >= maxNeg) break;
//		}
		return new Pair<List<RelationAndMentions>, List<RelationAndMentions>>(newTrain, test);
	}
	
	public static void computeTfIdf(List<RelationAndMentions> relMentions, 
			String idfFile) throws IOException {
		Map<String, Integer> counts = new HashMap<>();
		for(RelationAndMentions relMention : relMentions) {
			for(String midPhrase : NYTCounts.getTypedMidPhrases(relMention)) {
				if(midPhrase.contains("LONG")) continue;
				for(String str : getTokensFromFeature(midPhrase)) {
					if(!counts.containsKey(str)) {
						counts.put(str, 0);
					}
					counts.put(str, counts.get(str)+1);
				}
			}
		}
		System.out.println("Writing to idf file ...");
		FileUtils.writeStringToFile(new File(idfFile), "");
		String str = "";
		for(String token : counts.keySet()) {
			if(counts.get(token) == 1) continue;
			str=token+"\t"+(1.0/counts.get(token))+"\n";
			FileUtils.writeStringToFile(new File(idfFile), str, true);
		}
	}
	
	public static Double getSimilarity(String pattern1, String pattern2, 
			Map<String, Double> idfMap) {
		if(pattern1.equals(pattern2)) return 1.0;
		List<String> tokens1 = getTokensFromFeature(pattern1);
		List<String> tokens2 = getTokensFromFeature(pattern2);
		double intersectionScore = 0.0;
		double unionScore = 0.0;
		for(String tok1 : tokens1) {
			if(idfMap.containsKey(tok1)) {
				unionScore += idfMap.get(tok1);
			}
			if(tokens2.contains(tok1) && idfMap.containsKey(tok1)) {
				intersectionScore += idfMap.get(tok1);
			}
		}
		for(String tok2 : tokens2) {
			if(!tokens1.contains(tok2) && idfMap.containsKey(tok2)) {
				unionScore += idfMap.get(tok2);
			}
		}
		if(unionScore < 0.0000001) return 0.0;
		return intersectionScore / unionScore;
	}
}
