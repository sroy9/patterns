package subhro.counts;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;

import subhro.utils.Params;
import edu.stanford.nlp.ie.machinereading.structure.RelationMention;
import edu.stanford.nlp.kbp.slotfilling.MultiR;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset.Mention;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset.RelationAndMentions;
import edu.stanford.nlp.stats.ClassicCounter;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.util.Pair;
import edu.stanford.nlp.util.Triple;

public class NYTCounts {
	
	public static List<Set<String>> getGoldEntPairs(
			List<RelationAndMentions> relMentions) throws Exception {
	    List<Set<String>> labels = new ArrayList<Set<String>>();
		for(RelationAndMentions rel: relMentions) {
			labels.add(rel.posLabels);
	    }
		return labels;
	}
	
	public static Set<String> getTypedMidPhrases(RelationAndMentions relMention) {
		Set<String> typedMidPhrases = new HashSet<String>();
		for(Mention mention : relMention.mentions) {
			typedMidPhrases.addAll(getTypedMidPhrases(mention));
	    }
		return typedMidPhrases;
	}
	
	public static Set<String> getTypedMidPhrases(Mention mention) {
		Set<String> typedMidPhrases = new HashSet<String>();
		for(String midPhrase : mention.features.subList(0, 4)) {
			typedMidPhrases.addAll(Idf.getTokensFromFeature(midPhrase));
		}
		return typedMidPhrases;
	}
	
	public static Map<Pair<String, String>, Integer> getGrid(
			List<RelationAndMentions> relMentions) throws Exception {
		Map<Pair<String, String>, Integer> grid = new HashMap<Pair<String, String>, Integer>();
		for(RelationAndMentions relMention : relMentions) {
		    	Set<String> typedMidPhrases = getTypedMidPhrases(relMention);
		    	if(relMention.posLabels.size() == 0) relMention.posLabels.add("NA");
			for(String midPhrase : typedMidPhrases) {
				for(String relation : relMention.posLabels) {
					if(!grid.containsKey(new Pair<String, String>(relation, midPhrase))) {
						grid.put(new Pair<String, String>(relation, midPhrase), 0);
					}
					int cnt = grid.get(new Pair<String, String>(relation, midPhrase));
					grid.put(new Pair<String, String>(relation, midPhrase), cnt+1);
				}
			}
		}
		System.out.println(grid.size());
		System.out.println("Cleaning counts of 1 ...");
		Set<Pair<String,String>> sparsePhrases = new HashSet<Pair<String,String>>();
		for(Pair<String,String> key : grid.keySet()) {
			if(grid.get(key) <= 1) {
				sparsePhrases.add(key);
			}
		}
		for(Pair<String,String> key : sparsePhrases) {
			grid.remove(key);
		}
		System.out.println(grid.size());
		return grid;	
	}
	
	public static void createCountsFile(Map<Pair<String,String>,Integer> grid, String countsFile) 
			throws Exception {
		Map<String, Integer> midPhraseCounts = new HashMap<String, Integer>();
		for(Pair<String,String> key : grid.keySet()) {
			if(!midPhraseCounts.containsKey(key.second())) {
				midPhraseCounts.put(key.second(), 0);
			}
			midPhraseCounts.put(key.second(), 
					midPhraseCounts.get(key.second())+grid.get(key));
		}
		Map<Pair<String,String>, List<Double>> midPhraseAccuracy = 
				new HashMap<Pair<String,String>, List<Double>>();
		for(Pair<String,String> key : grid.keySet()) {
			if(key.first().equals("NA")) continue;
			if(key.second().contains("LONG")) continue;
			if(key.second().contains("__")) continue;
			midPhraseAccuracy.put(key, new ArrayList<Double>());
			midPhraseAccuracy.get(key).addAll(Arrays.asList(
					1.0*grid.get(key),
					1.0*midPhraseCounts.get(key.second()),
					grid.get(key)*1.0/midPhraseCounts.get(key.second())));
		}
		List<Entry<Pair<String,String>, List<Double>>> entryList = 
				new ArrayList<Entry<Pair<String,String>, List<Double>>>(midPhraseAccuracy.entrySet());
		Collections.sort(entryList, new Comparator<Entry<Pair<String,String>, List<Double>>>() {
			@Override
			public int compare(Entry<Pair<String,String>, List<Double>> o1, 
					Entry<Pair<String,String>, List<Double>> o2) {
				return (int) Math.signum(o2.getValue().get(2) - o1.getValue().get(2));
			}
		});
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(countsFile)));
		for(Entry<Pair<String,String>, List<Double>> entry : entryList) {
			bw.write(entry.getKey().first()+"\t"+entry.getKey().second()+"\t"+
					entry.getValue().get(0)+"\t"+entry.getValue().get(1)+"\t"+
					entry.getValue().get(2)+"\n");
		}
		bw.close();
	}
	
	public static Map<Pair<String,String>, List<Double>> readCountsFile(String countsFile) 
			throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(new File(countsFile)));
		String str;
		Map<Pair<String,String>, List<Double>> phraseScores = new HashMap<Pair<String,String>, List<Double>>();
		while((str = br.readLine())!=null) {
			String strArr[] = str.split("\t");
			phraseScores.put(new Pair<String, String>(strArr[0], strArr[1]), new ArrayList<Double>());
			phraseScores.get(new Pair<String, String>(strArr[0], strArr[1])).addAll(Arrays.asList(
					Double.parseDouble(strArr[2]),
					Double.parseDouble(strArr[3]),
					Double.parseDouble(strArr[4])));
		}
		br.close();
		return phraseScores;
	}
	
	public static List<Counter<String>> getAggregateSortedEntityPairs(
			List<RelationAndMentions> relMentions, 
			String countsFile) throws Exception {
		List<Counter<String>> predRelations = new ArrayList<Counter<String>>();
		Map<Pair<String,String>, List<Double>> phraseScores = readCountsFile(countsFile);
		for(RelationAndMentions relMention : relMentions) {
			Set<String> typedMidPhrases = getTypedMidPhrases(relMention);
			Counter<String> counter = new ClassicCounter<String>();
			for(String relation : Params.allNytRelations) {
				double max = -1.0;
				double sum = 0.0;
				for(String phrase : typedMidPhrases) {
					if(phrase.contains("LONG")) continue;
					if(phraseScores.containsKey(new Pair<String, String>(relation, phrase)) && 
							max < phraseScores.get(new Pair<String, String>(relation, phrase)).get(2)) {
						max = phraseScores.get(new Pair<String, String>(relation, phrase)).get(2);
					}
					if(phraseScores.containsKey(new Pair<String, String>(relation, phrase))) {
						sum += phraseScores.get(new Pair<String, String>(relation, phrase)).get(2);
					}
				}
				if(sum > 0.0) {
					counter.setCount(relation, sum);
				}
			}
			predRelations.add(counter);
		}
		return predRelations;
	}
	
	public static void runAccuracyBaseline() throws Exception {
		String trainFile = null, testFile = null;
		Map<Pair<String,String>,Integer> grid = null;
		trainFile = Params.riedelTrainFile;
		testFile = Params.riedelTestFile;
		InputStream is = new GZIPInputStream(new BufferedInputStream(new FileInputStream(trainFile)));
		List<RelationAndMentions> train = ProtobufToMultiLabelDataset.toRelations(is, false);
		System.out.println("Train size : "+train.size());
		is.close();
		is = new GZIPInputStream(new BufferedInputStream(new FileInputStream(testFile)));
		List<RelationAndMentions> test = ProtobufToMultiLabelDataset.toRelations(is, false);
		System.out.println("Test size : "+test.size());
		is.close();
		System.out.println("Creating grid ... ");
		grid = getGrid(train);
		System.out.println("Creating accuracy file ... ");
		createCountsFile(grid, Params.nytPatternsFile);
		System.out.println("Extracting gold ... ");
		List<Set<String>> gold = getGoldEntPairs(test);
		System.out.println("Extract entity pairs ...");
		List<Counter<String>> predicted = getAggregateSortedEntityPairs(test, Params.nytPatternsFile);
		List<Triple<Integer, String, Double>> preds = MultiR.convertToSorted(predicted);
		System.out.println("Writing to file ...");
		PrintStream os = new PrintStream(new FileOutputStream(Params.resultFile));
	    MultiR.generatePRCurveNonProbScores(os, gold, preds);
	    os.close();
	}
	
	public static void getSentenceScoresWithCounts() throws Exception {
	    FileUtils.write(new File(Params.sentenceLabelFile), "");
	    FileUtils.write(new File(Params.proofFile), "");
		String trainFile = null, testFile = null;
		Map<Pair<String,String>,Integer> grid = null;
		trainFile = Params.riedelTrainFile;
		testFile = Params.riedelTestFile;
		InputStream is = new GZIPInputStream(new BufferedInputStream(new FileInputStream(trainFile)));
		List<RelationAndMentions> train = ProtobufToMultiLabelDataset.toRelations(is, false);
		System.out.println("Train size : "+train.size());
		is.close();
		is = new GZIPInputStream(new BufferedInputStream(new FileInputStream(testFile)));
		List<RelationAndMentions> test = ProtobufToMultiLabelDataset.toRelations(is, false);
		System.out.println("Test size : "+test.size());
		is.close();
		
		// The following section is for using Ming-wei split
//		edu.illinois.cs.cogcomp.core.datastructures.Pair<List<RelationAndMentions>, List<RelationAndMentions>> pair = 
//				Idf.getMingWeiSplit(train, test, "/people/person/place_of_birth");
//		train = pair.getFirst();
//		test = pair.getSecond();
		// Ming-wei split ends here
		
		System.out.println("Creating grid ... ");
		grid = getGrid(train);
		System.out.println("Creating accuracy file ... ");
		createCountsFile(grid, Params.nytPatternsFile);
//		System.out.println("Creating idf file ... ");
//		Idf.computeTfIdf(train, Params.idfFile);
		Map<String, Map<String, List<Double>>> phraseScores = convert(readCountsFile(Params.nytPatternsFile));
//		Map<String, Double> idf = Reader.getIdfMap();
		System.out.println("Printing sentence scores ...");
		for(RelationAndMentions relMention : test) {
		    Map<String, Pair<Integer, Double>> bestSentenceForRelation = 
		    		new HashMap<String, Pair<Integer, Double>>();
		    for(int i=0; i<relMention.mentions.size(); ++i) {
		    		Mention mention = relMention.mentions.get(i);
				Set<String> typedMidPhrases = getTypedMidPhrases(mention);
				for(String relation : Params.evalRelations) {
					double max = -1.0;
					double sum = 0.0;
					double sim = 0.0;
					// To speed up, bring all relation stuff in one map
					Map<String, List<Double>> phraseScoresForRelation = phraseScores.get(relation);
					// Iterate over features, and compute scores
					for(String phrase : typedMidPhrases) {
						double maxSim = 0.0; 
						if(phrase.contains("LONG")) continue;
						if(phraseScoresForRelation.containsKey(phrase) && 
								max < phraseScoresForRelation.get(phrase).get(2)) {
							max = phraseScoresForRelation.get(phrase).get(2);
						}
						if(phraseScoresForRelation.containsKey(phrase)) {
							sum += phraseScoresForRelation.get(phrase).get(2);
						}
//						for(String phraseForRel : phraseScoresForRelation.keySet()) {
//							if((Idf.getSimilarity(phraseForRel, phrase, idf)*
//									phraseScoresForRelation.get(phraseForRel).get(2))>maxSim) {
//								maxSim = (Idf.getSimilarity(phraseForRel, phrase, idf)*
//										phraseScoresForRelation.get(phraseForRel).get(2));
//							}
//						}
//						sim += maxSim;
					}
					if(!bestSentenceForRelation.containsKey(relation) || 
							bestSentenceForRelation.get(relation).second() < sum) {
						bestSentenceForRelation.put(relation, new Pair<Integer, Double>(i, sum));
					}
				}
		    }
			String str = "";  
			for(String key : bestSentenceForRelation.keySet()) {
				if(key.equals(RelationMention.UNRELATED)) continue;
				str += key+"\t"+bestSentenceForRelation.get(key).first()+"\t"+
						bestSentenceForRelation.get(key).second()+"\n";
			}
			str+="\n";
			FileUtils.write(new File(Params.sentenceLabelFile), str, true);
		}
		MultiR.printProofs(test);
	}
	
	public static Map<String, Map<String, List<Double>>> convert(
			Map<Pair<String, String>, List<Double>> phraseScores) {
		Map<String, Map<String, List<Double>>> phraseScoresForRelation = 
				new HashMap<String, Map<String, List<Double>>>();
		for(Pair<String, String> key : phraseScores.keySet()) {
			if(!phraseScoresForRelation.containsKey(key.first())) {
				phraseScoresForRelation.put(key.first(), new HashMap<String, List<Double>>());
			}
			if(!phraseScoresForRelation.get(key.first()).keySet().contains(key.second())) {
				phraseScoresForRelation.get(key.first()).put(key.second(), phraseScores.get(key));
			}
		}
		return phraseScoresForRelation;
	}
	
	public static void main(String args[]) throws Exception {
		System.out.println("Running NYTCounts ");
		getSentenceScoresWithCounts();
//		runAccuracyBaseline();
	}
}
