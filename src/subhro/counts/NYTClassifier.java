package subhro.counts;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;

import subhro.utils.Params;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.sl.core.AbstractFeatureGenerator;
import edu.illinois.cs.cogcomp.sl.core.AbstractInferenceSolver;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.core.IStructure;
import edu.illinois.cs.cogcomp.sl.core.SLModel;
import edu.illinois.cs.cogcomp.sl.core.SLParameters;
import edu.illinois.cs.cogcomp.sl.core.SLProblem;
import edu.illinois.cs.cogcomp.sl.learner.Learner;
import edu.illinois.cs.cogcomp.sl.learner.LearnerFactory;
import edu.illinois.cs.cogcomp.sl.util.IFeatureVector;
import edu.illinois.cs.cogcomp.sl.util.Lexiconer;
import edu.illinois.cs.cogcomp.sl.util.WeightVector;
import edu.stanford.nlp.ie.machinereading.structure.RelationMention;
import edu.stanford.nlp.kbp.slotfilling.MultiR;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset.Mention;
import edu.stanford.nlp.kbp.slotfilling.multir.ProtobufToMultiLabelDataset.RelationAndMentions;

class X implements IInstance {
	public Mention mention;
	public String relation;
	
	public X(Mention mention, String rel) {
		this.mention = mention;
		relation = rel;
	}
}

class Y implements IStructure {
	public boolean result;
	public Y(boolean r) {
		result = r;
	}
}

class InfSolver extends AbstractInferenceSolver implements Serializable {

	private static final long serialVersionUID = -4766861938469502982L;
	private AbstractFeatureGenerator featGen;
	
	public InfSolver(AbstractFeatureGenerator featGen) throws Exception {
		this.featGen = featGen;
	}
	
	@Override
	public IStructure getBestStructure(WeightVector arg0, IInstance arg1)
			throws Exception {
		return getLossAugmentedBestStructure(arg0, arg1, null);
	}

	@Override
	public float getLoss(IInstance arg0, IStructure arg1, IStructure arg2) {
		Y gold = (Y) arg1;
		Y pred = (Y) arg2;
		if(gold.result == pred.result) return 0.0f;
		return 1.0f;
	}

	@Override
	public IStructure getLossAugmentedBestStructure(WeightVector arg0,
			IInstance arg1, IStructure arg2) throws Exception {
		if(arg0.dotProduct(featGen.getFeatureVector(arg1, new Y(true))) > 
			arg0.dotProduct(featGen.getFeatureVector(arg1, new Y(false)))) {
			return new Y(true);
		}
		return new Y(false);
	}
}

class FeatGen extends AbstractFeatureGenerator implements Serializable {

	private static final long serialVersionUID = 5304036779659761102L;
	public Lexiconer lm = null;
	
	public FeatGen(Lexiconer lm) {
		this.lm = lm;
	}

	@Override
	public IFeatureVector getFeatureVector(IInstance arg0, IStructure arg1) {
		X x = (X) arg0;
		Y y = (Y) arg1;
		Set<String> typedMidPhrases = NYTCounts.getTypedMidPhrases(x.mention);
		List<Pair<String, Double>> features = new ArrayList<Pair<String, Double>>();
		for(String feat : typedMidPhrases) {
			if(feat.contains("LONG")) continue;
			features.add(new Pair<String, Double>(y.result+"_"+x.relation+"_"+feat, 1.0));
		}
		return subhro.utils.FeatGen.getFeatureVectorFromListPair(features, lm);
	}
	
}

public class NYTClassifier {
	
	public static void main(String args[]) throws Exception {
	    FileUtils.write(new File(Params.sentenceLabelFile), "");
	    FileUtils.write(new File(Params.proofFile), "");
		String trainFile = null, testFile = null;
		trainFile = Params.riedelTrainFile;
		testFile = Params.riedelTestFile;
		InputStream is = new GZIPInputStream(new BufferedInputStream(new FileInputStream(trainFile)));
		List<RelationAndMentions> train = ProtobufToMultiLabelDataset.toRelations(is, false);
		System.out.println("Train size : "+train.size());
		is.close();
		is = new GZIPInputStream(new BufferedInputStream(new FileInputStream(testFile)));
		List<RelationAndMentions> test = ProtobufToMultiLabelDataset.toRelations(is, false);
		System.out.println("Test size : "+test.size());
		is.close();
		
//		The following section is for using Ming-wei split
//		edu.illinois.cs.cogcomp.core.datastructures.Pair<List<RelationAndMentions>, 
//		List<RelationAndMentions>> pair = 
//				Idf.getMingWeiSplit(train, test, "/people/person/place_of_birth");
//		train = pair.getFirst();
//		test = pair.getSecond();
//		Ming-wei split ends here
		
		SLModel model = new SLModel();
		Lexiconer lm = new Lexiconer();
		lm.setAllowNewFeatures(true);
		model.lm = lm;
		AbstractFeatureGenerator fg = new FeatGen(lm);
		model.featureGenerator = fg;
		model.infSolver = new InfSolver(fg);
		SLParameters para = new SLParameters();
		para.loadConfigFile(Params.spConfigFile);
		Learner learner = LearnerFactory.getLearner(model.infSolver, fg, para);
		SLProblem trainSL = new SLProblem();
		int numPos = 0, numNeg = 0;
		for(RelationAndMentions relMention : train) {
			for(String relation : Params.evalRelations) {
				if(relMention.posLabels.contains(relation)) {
					for(Mention mention : relMention.mentions) {
						trainSL.addExample(new X(mention, relation), new Y(true));
						numPos++;
					}
				} else if(Math.random() < 0.1){
					for(Mention mention : relMention.mentions) {
						trainSL.addExample(new X(mention, relation), new Y(false));
						numNeg++;
					}
				}
			}
		}
		System.out.println("Pos / Neg = "+numPos+" / "+numNeg);
		
		System.out.println("Learning ...");
		model.wv = learner.train(trainSL);
		
		lm.setAllowNewFeatures(false);
		for(RelationAndMentions relMention : test) {
			Map<String, Pair<Integer, Double>> bestSentenceForRelation = 
					new HashMap<String, Pair<Integer, Double>>();
			for(int i=0; i<relMention.mentions.size(); ++i) {
				Mention mention = relMention.mentions.get(i);
				for(String relation : Params.evalRelations) {
					double score = model.wv.dotProduct(model.featureGenerator.getFeatureVector(
							new X(mention, relation), new Y(true)));
					// SUBHRO : Get the best sentence for each relation
			        if(!bestSentenceForRelation.containsKey(relation) || 
			        		bestSentenceForRelation.get(relation).getSecond() < score) {
			        		bestSentenceForRelation.put(relation, new Pair<Integer, Double>(i, score));
			        }
			    }
			}
			String str = "";  
			try {
				for(String key : bestSentenceForRelation.keySet()) {
					if(key.equals(RelationMention.UNRELATED)) continue;
					str += key+"\t"+bestSentenceForRelation.get(key).getFirst()+"\t"+
							bestSentenceForRelation.get(key).getSecond()+"\n";
				}
				str+="\n";
				FileUtils.write(new File(Params.sentenceLabelFile), str, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		MultiR.printProofs(test);
	}
	
//	public static double getPatternScore(String relation, Mention mention,
//			Map<String, Map<String, List<Double>>> phraseScores) throws Exception {
//		Set<String> typedMidPhrases = NYTCounts.getTypedMidPhrases(mention);
//		double sum = 0.0;
//		// To speed up, bring all relation stuff in one map
//		Map<String, List<Double>> phraseScoresForRelation = phraseScores.get(relation);
//		// Iterate over features, and compute scores
//		for(String phrase : typedMidPhrases) {
//			if(phrase.contains("LONG")) continue;
//			if(phraseScoresForRelation.containsKey(phrase)) {
//				sum += phraseScoresForRelation.get(phrase).get(2);
//			}
//		}
//		return sum;
//	}
	
//	public static double getFeatureFrequency(String relation, String pattern,
//			Map<edu.stanford.nlp.util.Pair<String, String>, List<Double>> phraseScores) {
//		if(phraseScores.containsKey(new edu.stanford.nlp.util.Pair<String, String>(relation, pattern))) {
//			return phraseScores.get(new edu.stanford.nlp.util.Pair<String, String>(relation, pattern)).get(1);
//		} else {
//			return 1.0;
//		}
//	}
	
}