package subhro.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;

public class Reader {
	
	public static Map<String, Boolean> getGoldAnnotatedTuples(String relation) 
			throws IOException {
		List<String> lines = FileUtils.readLines(new File(Params.goldNytFile));
		Map<String, Boolean> gold = new HashMap<>();
		for(String line : lines) {
			String strArr[] = line.split("\t");
			if(!relation.equals(strArr[0])) continue;
			String entPairSent = strArr[1]+"|||"+strArr[2]+"|||"+strArr[3];
			Boolean answer = Boolean.parseBoolean(strArr[4]);
			gold.put(entPairSent, answer);
		}
		return gold;
	}
	
	public static Map<Pair<String,List<String>>, Double> getProofTuples(
			String fileName, String relation) throws IOException {
		List<String> lines = FileUtils.readLines(new File(fileName));
		Map<Pair<String,List<String>>, Double> tuples = new HashMap<>();
		for(String line : lines) {
			String strArr[] = line.split("\t");
			if(!relation.equals(strArr[1])) continue;
			Double score = Double.parseDouble(strArr[0]);
			String entPairSent = strArr[2]+"|||"+strArr[3]+"|||"+strArr[4];
			List<String> feats = new ArrayList<>();
			feats.addAll(Arrays.asList(strArr[5],strArr[6],strArr[7],strArr[8]));
			tuples.put(new Pair<String, List<String>>(entPairSent, feats), score);
		}
		return tuples;
	}
	
	public static Map<String, Double> getIdfMap() throws IOException {
		List<String> lines = FileUtils.readLines(new File(Params.idfFile));
		Map<String, Double> gold = new HashMap<>();
		for(String line : lines) {
			String strArr[] = line.split("\t");
			gold.put(strArr[0], Double.parseDouble(strArr[1]));
		}
		return gold;
	}
}
