package subhro.utils;

import java.util.Arrays;
import java.util.List;

public class Params {
	
	public static String riedelTrainFile = "corpora/multir/train-Multiple.pb.gz";
	public static String riedelTestFile = "corpora/multir/test-Multiple.pb.gz";
//	public static String spConfigFile = "config/DCD.config";
	public static String spConfigFile = "config/StructuredPerceptron.config";
	
	public static List<String> allNytRelations = Arrays.asList(
			"/time/event/locations",
			"/people/family/country",
			"/people/family/members",
			"/people/person/children",
			"/people/person/religion",
			"/people/person/ethnicity",
			"/business/person/company",
			"/people/person/profession",
			"/location/country/capital",
			"/location/province/capital",
			"/location/us_state/capital",
			"/location/de_state/capital",
			"/location/mx_state/capital",
			"/location/br_state/capital",
			"/people/person/place_lived",
			"/business/company/advisors",
			"/business/company/founders",
			"/people/person/nationality",
			"/business/company/locations",
			"/broadcast/content/location",
			"/location/fr_region/capital",
			"/location/it_region/capital",
			"/location/location/contains",
			"/broadcast/producer/location",
			"/film/film_festival/location",
			"/sports/sports_team/location",
			"/people/person/place_of_birth",
			"/location/cn_province/capital",
			"/business/shopping_center/owner",
			"/business/company/place_founded",
			"/location/us_county/county_seat",
			"/location/jp_prefecture/capital",
			"/people/ethnicity/includes_groups",
			"/film/film/featured_film_locations",
			"/location/in_state/judicial_capital",
			"/people/ethnicity/included_in_group",
			"/business/company/major_shareholders",
			"/film/film_location/featured_in_films",
			"/people/deceased_person/place_of_death",
			"/location/neighborhood/neighborhood_of",
			"/location/in_state/legislative_capital",
			"/people/deceased_person/place_of_burial",
			"/people/place_of_interment/interred_here",
			"/people/ethnicity/geographic_distribution",
			"/location/administrative_division/country",
			"/location/in_state/administrative_capital",
			"/location/country/administrative_divisions",
			"/business/business_location/parent_company",
			"/business/company_advisor/companies_advised",
			"/people/profession/people_with_this_profession",
			"/business/shopping_center_owner/shopping_centers_owned"
			);
	
	// default values for the parameters
	public static String resultFile = "result.tsv";
	public static String sentenceLabelFile = "sentenceLabel.tsv";
	public static String proofFile = "proof.tsv";
	
	public static List<String> evalRelations = Arrays.asList(
			"/location/location/contains",
			"/business/person/company",
			"/people/person/nationality",
			"/people/person/place_lived",      
			"/location/neighborhood/neighborhood_of",
			"/people/deceased_person/place_of_death",
			"/people/person/place_of_birth",
			"/business/company/founders",
			"/people/person/children",
			"/location/administrative_division/country",
			"/business/company/major_shareholders"
			);
	public static String goldNytFile = "corpora/goldNytFile.tsv";
	public static String idfFile = "idf.tsv";
	public static String nytPatternsFile = "nyt.sorted";
	public static int poolDepth = 50;
	public static int runDepth = 1000;
	public static String tableFile = "table.tsv";
	public static String trainFile = "train.tsv";
	public static String weightFile = "weight.tsv";
}
