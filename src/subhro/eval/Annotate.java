package subhro.eval;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;

import subhro.utils.Params;
import subhro.utils.Reader;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;

public class Annotate {

	public static String modifyString(String relation, String sentence, List<String> features) {
		String strArr[] = features.get(3).split("\\|");
		String q = relation+"\t"+strArr[0]+"\t";
		String tokens[] = sentence.split(" ");
		Map<Integer, String> markers = new HashMap<Integer, String>();
		boolean found = false;
		for(int i=0; i<tokens.length; ++i) {
			for(int j=i+1; j<=tokens.length; ++j) {
				for(int k=j; k<=tokens.length; ++k) {
					for(int l=k+1; l<=tokens.length; ++l) {
						String pre = "", post = "", mid = "";
						if(i==0) pre="B_-2 B_-1"; 
						else if(i==1) pre = "B_-1 "+tokens[0];
						else pre = tokens[i-2]+" "+tokens[i-1];
						if(l==tokens.length) post="B_2 B_1"; 
						else if(l==tokens.length-1) post = tokens[l]+" B_1";
						else post = tokens[l]+" "+tokens[l+1];
						for(int m=j; m<k; ++m) {
							mid+=tokens[m]+" ";
						}
						mid = mid.trim();
						if(pre.equals(strArr[1]) && (mid.equals(strArr[3]) || strArr[3].contains("LONG")) 
								&& post.equals(strArr[5])) {
							found = true;
							markers.put(i, " [[ ");
							markers.put(j, " ]] ");
							markers.put(k, " [[ ");
							markers.put(l, " ]] ");
							break;
						}
					}
					if(found) break;
				}
				if(found) break;
			}
			if(found) break;
		}
		for(int i=0; i<tokens.length; ++i) {
			if(markers.containsKey(i)) {
				q += markers.get(i);
			}
			q += tokens[i]+" ";
		}
		if(markers.containsKey(tokens.length)) {
			q += markers.get(tokens.length);
		}
		q += "\n\nResponse : ";
		return q;
	}
	
	public static void annotate(String relation, String[] outputFiles, BufferedReader br) 
			throws IOException {
		List<Entry<Pair<String, List<String>>, Double>> pool = Eval.getPoolEntries(relation, outputFiles);
		Map<String, Boolean> goldMap = Reader.getGoldAnnotatedTuples(relation);
		for(Entry<Pair<String, List<String>>, Double> entry : pool) {
			if(!goldMap.containsKey(entry.getKey().getFirst())) {
				String strArr[] = entry.getKey().getFirst().split("\\|\\|\\|");
				String sentence = strArr[2];
				System.out.print(modifyString(relation, sentence, entry.getKey().getSecond()));
				String response = br.readLine().trim();
				System.out.println("Response recorded : "+response+"\n\n");
				if(response.equals("y") || response.equals("Y")) {
					FileUtils.write(new File(Params.goldNytFile), 
							relation+"\t"+strArr[0]+"\t"+strArr[1]+"\t"+strArr[2]+"\ttrue\n", true);
					goldMap.put(strArr[0]+"|||"+strArr[1]+"|||"+strArr[2], true);
				} else if(response.equals("n") || response.equals("N")){
					FileUtils.write(new File(Params.goldNytFile), 
							relation+"\t"+strArr[0]+"\t"+strArr[1]+"\t"+strArr[2]+"\tfalse\n", true);
					goldMap.put(strArr[0]+"|||"+strArr[1]+"|||"+strArr[2], false);
				} else {
					System.out.println("Exiting");
					System.exit(0);
				}
			}
		}
	}
	
	public static void annotate(String[] outputFiles, BufferedReader br) throws IOException {
		for(String relation : Params.evalRelations) {
			annotate(relation, outputFiles, br);
		}
	}
	
 	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 		annotate(args, br);
 		br.close();
 	}
}
