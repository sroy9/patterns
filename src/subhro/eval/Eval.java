package subhro.eval;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import subhro.utils.Params;
import subhro.utils.Reader;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;

/*
 * This class takes output files from multiple systems, and outputs MAP and WMAP
 * Assumes the top k for each relation from each file has been annotated and 
 * is present in data/gold.txt
 */

public class Eval {
	
	public static boolean contains(Set<String> big, List<String> small) {
		for(String str : small) {
			if(!big.contains(str)) return  false;
		}
		return false;
	}
	
	public static Set<String> getGoldEntPairSentences(Map<String, Boolean> map) {
		Set<String> positives = new HashSet<>();
		for(String key : map.keySet()) {
			if(map.get(key)) {
				positives.add(key);
			}
		}
		return positives;
	}
	
	public static double computeAvgPrecision(Set<String> relevant, List<String> retrieved) {
		int found = 0;
		double avgPrecision = 0.0;
		for(int i=0; i<retrieved.size(); ++i) {
			String doc = retrieved.get(i);
			if(relevant.contains(doc)) {
				found++;
				avgPrecision += (found*1.0/(i+1));
				if(found == relevant.size()) break;
			}
		}
		return avgPrecision / relevant.size();
	}
	
	public static Set<String> getPool(String relation, String[] outputFiles) throws IOException {
		Set<String> pool = new HashSet<>();
		for(String file : outputFiles) {
			Map<Pair<String, List<String>>, Double> tuples = 
					Reader.getProofTuples(file, relation);
			List<Entry<Pair<String, List<String>>, Double>> entryList = new ArrayList<>();
			entryList.addAll(tuples.entrySet());
			Collections.sort(entryList, new Comparator<Entry<Pair<String, List<String>>, Double>>() {
				@Override
				public int compare(
						Entry<Pair<String, List<String>>, Double> o1,
						Entry<Pair<String, List<String>>, Double> o2) {
					return (int) Math.signum(o2.getValue() - o1.getValue());
				}
			});
			for(int i=0; i<Params.poolDepth; ++i) {
				Entry<Pair<String, List<String>>, Double> entry = entryList.get(i);
				pool.add(entry.getKey().getFirst());
			}
		}
		return pool;
	}
	
	public static List<Entry<Pair<String, List<String>>, Double>> getPoolEntries(
			String relation, String[] outputFiles) throws IOException {
		List<Entry<Pair<String, List<String>>, Double>> pool = new ArrayList<>();
		for(String file : outputFiles) {
			Map<Pair<String, List<String>>, Double> tuples = 
					Reader.getProofTuples(file, relation);
			List<Entry<Pair<String, List<String>>, Double>> entryList = new ArrayList<>();
			entryList.addAll(tuples.entrySet());
			Collections.sort(entryList, new Comparator<Entry<Pair<String, List<String>>, Double>>() {
				@Override
				public int compare(
						Entry<Pair<String, List<String>>, Double> o1,
						Entry<Pair<String, List<String>>, Double> o2) {
					return (int) Math.signum(o2.getValue() - o1.getValue());
				}
			});
			for(int i=0; i<Params.poolDepth; ++i) {
				Entry<Pair<String, List<String>>, Double> entry = entryList.get(i);
				pool.add(entry);
			}
		}
		return pool;
	}
	
	public static Set<String> getGoldEntPairSentencesFromPool(
			String relation, String[] outputFiles) throws IOException {
		Set<String> pool = getPool(relation, outputFiles);
		Map<String, Boolean> goldMap = Reader.getGoldAnnotatedTuples(relation);
		Set<String> gold = getGoldEntPairSentences(goldMap);
		Set<String> goldFromPool = new HashSet<>();
		for(String str : pool) {
			if(!goldMap.containsKey(str)) {
				System.out.println("Required annotations for "+relation+" not present");
				System.exit(0);
			}
		}
		for(String str : gold) {
			if(pool.contains(str)) {
				goldFromPool.add(str);
			}
		}
		return goldFromPool;
	}
	
	public static void evaluate(String[] outputFiles) throws IOException {
		Map<Pair<String, String>, Pair<Double, Integer>> scores = 
				new HashMap<Pair<String,String>, Pair<Double, Integer>>();
		for(String relation : Params.evalRelations) {
			Set<String> goldEntPairSents = getGoldEntPairSentencesFromPool(relation, outputFiles);
			for(String file : outputFiles) {
				Map<Pair<String, List<String>>, Double> tuples = 
						Reader.getProofTuples(file, relation);
				List<Entry<Pair<String, List<String>>, Double>> entryList = new ArrayList<>();
				entryList.addAll(tuples.entrySet());
				Collections.sort(entryList, new Comparator<Entry<Pair<String, List<String>>, Double>>() {
					@Override
					public int compare(
							Entry<Pair<String, List<String>>, Double> o1,
							Entry<Pair<String, List<String>>, Double> o2) {
						return (int) Math.signum(o2.getValue() - o1.getValue());
					}
				});
				List<String> predEntPairSents = new ArrayList<>();
				for(int i=0; i<Params.runDepth; ++i) {
					Entry<Pair<String, List<String>>, Double> entry = entryList.get(i);
					predEntPairSents.add(entry.getKey().getFirst());
				}
				scores.put(new Pair<String, String>(relation, file), new Pair<Double, Integer>(
						computeAvgPrecision(goldEntPairSents, predEntPairSents), goldEntPairSents.size()));
			}
		}

		Map<String, Double> prec = new HashMap<String, Double>();
		Map<String, Double> wPrec = new HashMap<String, Double>();
		for(String file : outputFiles) {
			double p = 0, wp = 0, totRelevant = 0;
			for(String relation : Params.evalRelations) {
				Double score = scores.get(new Pair<String, String>(relation, file)).getFirst();
				int goldSize = scores.get(new Pair<String, String>(relation, file)).getSecond();
				p += score;
				wp += goldSize * score;
				totRelevant += goldSize;
			}
			prec.put(file, p / Params.evalRelations.size());
			wPrec.put(file, wp / totRelevant);
		}
		String str = "Relation\t";
		for(String file : outputFiles) {
			str += file+"\t";
		}
		str+="\n";
		for(String relation : Params.evalRelations) {
			str += relation+"\t"+getGoldEntPairSentencesFromPool(relation, outputFiles).size()+"\t";
			for(String file : outputFiles) {
				str += scores.get(new Pair<String, String>(relation, file)).getFirst()+"\t";
			}
			str += "\n";
		}
		str += "MAP\t";
		for(String file : outputFiles) {
			str += prec.get(file)+"\t";
		}
		str +="\nWMAP\t";
		for(String file : outputFiles) {
			str += wPrec.get(file)+"\t";
		}
		str +="\n";
		FileUtils.write(new File(Params.tableFile), str);
	}
	
	public static void analyze(String file1, String file2) throws IOException {
		for(String relation : Params.evalRelations) {
			Map<Pair<String, List<String>>, Double> tuples1 = 
					Reader.getProofTuples(file1, relation);
			Map<Pair<String, List<String>>, Double> tuples2 = 
					Reader.getProofTuples(file2, relation);
			for(Pair<String, List<String>> pair1 : tuples1.keySet()) {
				for(Pair<String, List<String>> pair2 : tuples2.keySet()) {
					String s1[] = pair1.getFirst().split("\\|\\|\\|");
					String s2[] = pair1.getFirst().split("\\|\\|\\|");
					if(s1[0].equals(s2[0]) && s1[1].equals(s2[1]) && !s1[2].equals(s2[2])) {
						System.out.println("Found : "+relation+"\n"+pair1.getFirst()+"\n"+pair2.getFirst());
					}
				}
			}
		}
	}
	
	public static void main(String args[]) throws IOException {
		evaluate(args);
//		analyze(args[0], args[1]);
	}

}
