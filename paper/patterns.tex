%
% File naaclhlt2016.tex
%

\documentclass[11pt,letterpaper]{article}
\usepackage{naaclhlt2016}
\usepackage{times}
\usepackage{latexsym}
\usepackage{amsmath}

%\naaclfinalcopy % Uncomment this line for the final submission
\def\naaclpaperid{***} %  Enter the naacl Paper ID here

% To expand the titlebox for more authors, uncomment
% below and set accordingly.
% \addtolength\titlebox{.5in}    

\newcommand\BibTeX{B{\sc ib}\TeX}


\title{Witnessed Relation Extraction}

% Author information can be set in various styles:
% For several authors from the same institution:
% \author{Author 1 \and ... \and Author n \\
%         Address line \\ ... \\ Address line}
% if the names do not fit well on one line use
%         Author 1 \\ {\bf Author 2} \\ ... \\ {\bf Author n} \\
% For authors from different institutions:
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \And  ... \And
%         Author n \\ Address line \\ ... \\ Address line}
% To start a seperate ``row'' of authors use \AND, as in
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \AND
%         Author 2 \\ Address line \\ ... \\ Address line \And
%         Author 3 \\ Address line \\ ... \\ Address line}
% If the title and author information does not fit in the area allocated,
% place \setlength\titlebox{<new height>} right after
% at the top, where <new height> can be something larger than 2.25in
\author{Author 1\\
      XYZ Company\\
          111 Anywhere Street\\
              Mytown, NY 10000, USA\\
                  {\tt author1@xyz.org}
                    \And
                    Author 2\\
                    ABC University\\
                    900 Main Street\\
                    Ourcity, PQ, Canada A1A 1T2\\
  {\tt author2@abc.ca}}

\date{}

\begin{document}

\maketitle

\begin{abstract}

  Standard knowledge base (KB) completion techniques involve obtaining
  new facts from relation extraction systems, and adding them to
  KB. However most extraction systems are not perfect, and do not
  ensure $100\%$ accuracy of the extracted facts. As a result, these
  facts cannot be directly used to enrich existing knowledge bases.
  In order to facilitate the reliable extraction of accurate facts
  from text corpora, we introduce the task of Witnessed Relation
  Extraction (WRE).  The goal of the task is to come up with both new
  facts as well as witnesses (proofs) that support the extracted
  facts. The coupling of facts with their witnesses allows for easy
  manual verification of extracted facts -- one just has to check
  whether the associated witness supports the fact. A human can easily
  go over the output of WRE and choose accurate facts to add to KB. We
  analyzed the performance of various methods on WRE. Surprisingly,
  simple pattern matching methods outperform state of the art (distant
  supervision based) relation extractors on the task. We release code
  and annotated datasets for future developements in WRE.

\end{abstract}

\section{Introduction}

  Knowledge bases like Freebase, YAGO, etc. are highly
  incomplete. Over $70\%$ of people included in Freebase have no known
  place of birth, and $99\%$ have no known ethnicity. The standard
  strategy for knowledge base completion is to extract new facts
  $(e_1, r, e_2)$ (where $e_1, e_2$ are entities, and $r$ is a
  relation which exists between $e_1$ and $e_2$) from a relation
  extraction system. However, most relation extraction systems are not
  precise, and hence, extracted facts from these systems cannot be
  reliably used for knowledge base completion.

  To achieve reliable extraction of new facts for KB completion, we
  introduce a new task - Witnessed Relation Extraction (WRE), which
  requires extraction of facts of the form $(e_1, r, e_2)$ coupled
  with witness $w$ to support it. The goal of the task is to simplify
  the manual verification of extracted facts. A verifier can quickly
  go over the output of a WRE system and prune noisy extractions,
  since he just has to check whether the associated witness supports
  the extracted fact. Without a witness, verification of facts is
  challenging, since each fact might involve searching the web
  separately. The verification is more difficult for facts about new
  emerging entities and from specialized domains, about which little
  information is available on the web.  The output of WRE directly
  addresses the problem of finding evidence for extracted facts.
  
  We finally develop various methods and analyzed their performance on
  WRE. Surprisingly a simple pattern matching baseline outperforms
  state of the art relation extraction systems on WRE task. Upon
  publication of this paper, we will release our code and annotated
  datasets for future developments of WRE.

  The next section describes the task more formally. We next describe
  various methods for WRE, and conclude with experimental results.

\section{Related Work}

  There has been a lot of work in extracting KB relations from a
  corpora of text. Most of these approaches learn models using
  different forms of distant supervision, and output a ranked list of
  extracted tuples. \cite{MBSJ09} assumes that if $(e_1, r, e_2) \in
  K$, all sentences mentioning $e_1$ and $e_2$ express the relation
  $r$. On the other hand, \cite{RiedelYaMc10} assumes at least one of
  the sentences mentioning $e_1$ and $e_2$ express the relation
  $r$. They model relation extraction as a multi-instance single-label
  problem, which allows multiple sentences for the same entity pair
  but disallows more than one relation for each entity
  pair. \cite{HZLZW11} and \cite{STNM12} develop multi-instance
  multi-label learning models, allowing both multiple sentences as
  well as multiple relations for each entity pair. Finally, \cite{}
  use matrix factorization methods to capture both KB redundancy and
  text patterns to discover new facts. However all the above
  approaches focus on finding new relation facts in isolation, without
  finding evidence for the extractions.

  A second strand of work involves the TAC KBP slot filler task, where
  the goal is to extract all relational facts $(e_1, r, e_2)$ given a
  query that contains only the first entity $e_1$, and a document in
  which the entity is mentioned. In addition to the extracted facts,
  the task required systems to report provenance for the extractions,
  which was a span of text from a set of documents. In contrast, our
  task does not provide any entity as input. Finally, WRE outputs a
  ranked list of extractions whereas the slot filler task evaluates on
  accuracy of extracted facts. Therefore, WRE can be seen as a bridge
  between the slot filler task and aforementioned distant supervision
  based relation extraction works.

\section{Task Definition}

  In this section, we describe the WRE task more formally. We consider
  only binary relations in this work, and a relation fact is
  represented as a triple $(e_1, r, e_2)$ indicating the relation $r$
  exists between entities $e_1$ and $e_2$. In terms of relation
  extraction, we define a knowledge base $\Sigma$ as follows $\Sigma =
  \{(e_i,r_j,e_k)\}$, where $e_i$ and $e_k$ are entities, and $r_j$ is
  the relation. Assume that $\Sigma^*$ is the gold knowledge base that
  contains all of the true relations in the world. As a result, we can
  say that a relation $r$ exists for the entity pair $e_1$ and $e_2$
  if

  $$(e_1,r,e_2) \in \Sigma^*$$.

  Then, for a given entity pair $e_1,e_2$ and a relation $r$, we would
  like to find a witness $\gamma_r$ which is a function of the entity
  pair $e_1$ and $e_2$, to imply the relation $r$. More formally,

  $$\gamma_r(e_1,e_2) \Rightarrow  ((e_1,r,e_2) \in \Sigma^*)$$

  The witness $\gamma_r$ can be any form of evidence for supporting
  the relation $r$. We define a witness learner $\theta$, as a
  function to score how well a witness $\gamma_r$ supports a fact
  $(e_1, r, e_2)$. Finally, assuming $\Gamma_r$ to be the set of all
  witnesses, we define

  $$P(r|e_1,e_2) \propto \max_{\gamma_r(e_1,e_2) \in \Gamma_r}
  \theta(e_1,e_2,\gamma_r(e_1,e_2))$$

  and the corresponding witness to be

  $$\omega_\theta^r(e_1,e_2) = \arg\max_{\gamma_r(e_1,e_2) \in
    \Gamma_r} \theta(e_1,e_2,\gamma_r(e_1,e_2))$$

  \noindent
  \textbf{Task Evaluation} Given as input relation $r$, we output a
  ranked list of facts $(e_1, r, r_2)$ based on the value of
  $P(r|e_1,e_2)$, and the corresponding $\omega_\theta^r(e_1,e_2)$ is
  provided as the witness for the extraction. The $(e_1,r,e_2)$ is
  considered to be correct only if
  $$\omega_\theta^r(e_1,e_2) \Rightarrow (e_1,r,e_2) \in \Sigma^*$$.

  \noindent
  \textbf{Choice of evidence} In this work, for an extraction
  involving entity pair $e_1, e_2$, the witness
  $\omega_\theta^r(e_1,e_2)$ has to be a sentence mentioning entities
  $e_1, e_2$. We make this choice because of two reasons. First, it is
  easy to verify for an evaluator, whether a sentence mentioning $e_1,
  e_2$ implies that a relation $r$ exists between $e_1, e_2$. Second,
  it is fair to rare entities, since it is likely that a relation
  involving some rare entity will be expressed in at least one
  sentence in the corpus.

\section{Approaches to WRE}

  In this section, we discuss various approaches to WRE.

  \subsection{Distant Supervision Models for WRE}
  We used the methods of \cite{MBSJ09} and \cite{STNM12} to WRE. These
  methods were designed to extract only facts, not witnesses. However
  both assign scores to individual sentences during prediction of a
  relation. We used these scores as the witness learner scores, and
  use thse to extract proofs for fact extraction.

  \subsection{Pattern based methods for WRE}
  Let $S$ be a sentence which mentions entities $e_1$ and $e_2$.  We
  define $\mathrm{Pat}(S, e_1, e_2)$ to be the set of text patterns
  extracted from sentence $S$ for the entity pair $e_1$ and $e_2$
  . Finally, for each text pattern $p$ and relation $r$, we define the
  following terms :
  \begin{align*}
    \mathrm{Counts}(r, p) &= |\{(S, e_1, e_2) | (e_1, r, e_2) \in K 
    \text{ and } \\
    & p \in \mathrm{Pat}(S, e_1, e_2)\}| \\
    \mathrm{Counts}(p) &= |\{(S, e_1, e_2) | 
    p \in \mathrm{Pat}(S, e_1, e_2)\}| \\
    \mathrm{Accuracy}(r, p) &= \frac{Counts(r, p)}{Counts(p)}
  \end{align*}  

  Note that if the distant supervision assumption is true (that is,
  when $(e_1, r, e_2) \in K$, all sentences mentioning $e_1$ and $e_2$
  express the relation $r$), $\mathrm{Accuracy}(r,p)$ can be
  interpreted as the probability that a pattern $p$ implies relation
  $r$. Finally, we use
  \begin{align*}
    \sum_{p \in \mathrm{Pat}(S, e_1, e_2)} \mathrm{Accuracy}(r, p)
  \end{align*}  
  as witness learner scores.

  We define $\mathrm{Pat}(S, e_1, e_2)$ to contain conjunctions
  of the following properties:
      \begin{itemize}
        \item The sequence of words between the two entities.
        \item A flag indicating which entity came first in the sentence.
        \item A window of $k$ words to the left of Entity 1.
        \item A window of $k$ words to the right of Entity 2.
        \item The NER tags of the entities.  
      \end{itemize}  
      We generate a conjunctive feature for each $k \in \{0, 1, 2\}$. In 
      addition, we also add a pattern constituting conjunction of the NER
      tags of the entities.
 
  We refer to this method as \textbf{SimplePat}. Since it tries to
  match entire sequence of words between the two entities,
  \textbf{SimplePat} is not robust to small edits of text. For
  example, if it has only seen ``is daughter of'' as a pattern for the
  relation /people/person/children, it will not be able to detect ``is
  eldest daughter of'' as a positive indicator at test time. In order to
  address the robustness issue of \textbf{SimplePat}, we propose two
  methods with slight modifications to the \textbf{SimplePat} routine.
  
  \begin{enumerate}

    \item \textbf{DecompPat} : In this case, we use the same set of
      patterns as \textbf{SimplePat}, except instead of using the
      entire sequence of words between the entities, we use bigrams
      between the entities. Table \ref{tab:patterns} shows an example
      of the patterns extracted from a sentence for \textbf{SimplePat}
      and \textbf{DecompPat}.

    \item  \textbf{SoftPatMatch} : In this case, we use the same patterns as
      \textbf{SimplePat} but modify the scoring function to allow for 
      soft matching of patterns. If $\mathcal{P}$ is the set of all
      patterns, the scoring function is defined as
  \begin{align*}
    \sum_{p \in \mathrm{Pat}(S, e_1, e_2)} \max_{p' \in \mathcal{P}}
    (sim(p, p') \times \mathrm{Accuracy}(r, p'))
  \end{align*}
  where $sim(p, p')$ is a similarity score for $p$ and $p'$.
 
  We define components $\mathcal{C}(p)$ of a pattern $p$ as set of
  unigrams, and NER tags from pattern $p$, and $\mathrm{Idf(c)}$ to be
  the inverse document frequency of a component $c$. Finally, we
  define the similarity function of two patterns $p$ and $p'$ as
  \begin{align*}
    sim(p,p') = \frac{\sum_{c \in \mathcal{C}(p) \cap 
        \mathcal{C}(p')}\mathrm{Idf}(c)}
    {\sum_{c \in \mathcal{C}(p) \cup
        \mathcal{C}(p')}\mathrm{Idf}(c)}
  \end{align*}  
 
  \end{enumerate}    

  \begin{table*}[!ht]
        \centering \footnotesize
        \begin{tabular}{|c|c|}
          \hline
           SimplePat & DecompPat \\
          \hline
          \hline
          PERSON-LOCATION & PERSON-LOCATION\\
          inverse\_false : PERSON was born in LOCATION & 
          inverse\_false : PERSON was born LOCATION \\
          inverse\_false : PERSON was born in LOCATION , & 
          inverse\_false : PERSON was born LOCATION ,\\
          inverse\_false : PERSON was born in LOCATION , Ontario &
          inverse\_false : PERSON was born LOCATION , Ontario\\
          & inverse\_false : PERSON born in LOCATION \\
          & inverse\_false : PERSON born in LOCATION ,\\
          & inverse\_false : PERSON born in LOCATION , Ontario\\
          \hline
        \end{tabular}
        \caption{\footnotesize Patterns extracted from ``[James
            Hillier$]_{e_1}$ was born in [Brantford$]_{e_2}$ , Ontario
          .''}
        \label{tab:patterns}
  \end{table*}  


  \begin{table*}[!ht]
        \centering \footnotesize
        \begin{tabular}{|ll|ccccc|}
          \hline
           Relation & \# & Mintz & Miml & SimplePat & DecompPat & SoftPatMatch \\
          \hline
/location/location/contains & 146 & 0.31 & 0.42 & 0.45 & 0.25 & 0.45\\
/business/person/company & 130 & 0.49 & 0.52 & 0.42 & 0.56 & 0.56\\
/people/person/nationality & 69 & 0.35 & 0.23 & 0.32 & 0.51 & 0.36\\
/people/person/place\_lived & 119 & 0.35 & 0.15 & 0.51 & 0.37 & 0.55\\
/location/neighborhood/neighborhood\_of & 28 & 0.12 & 0.13 & 0.27 & 0.28 & 0.33\\
/people/deceased\_person/place\_of\_death & 29 & 0.19 & 0.52 & 0.02 & 0.1 & 0.02\\
/people/person/place\_of\_birth & 32 & 0.03 & 0.06 & 0.21 & 0.43 & 0.39\\
/business/company/founders & 19 & 0.3 & 0.2 & 0.24 & 0.64 & 0.5\\
/people/person/children & 30 & 0.13 & 0.15 & 0.02 & 0.71 & 0.4\\
/location/administrative\_division/country & 58 & 0.08 & 0.09 & 0.28 & 0.15 & 0.3\\
/business/company/major\_shareholders & 6 & 0.07 & 0.0 & 0.0 & 0.01 & 0.0\\
\hline
MAP & & 0.22 & 0.23 & 0.25 & 0.37 & 0.35\\
WMAP & & 0.3 & 0.3 & 0.36 & 0.38 & 0.44\\
          \hline
        \end{tabular}
        \caption{\footnotesize Average and (weighted) Mean Average
          Precisions for Freebase relations based on pooled
          results. The \# column shows the number of true facts in the
          pool}
        \label{tab:map}
  \end{table*}
  

\section{Evaluation}

  We evaluate our methods on the corpora made available by
  \cite{RiedelYaMc10} and constructed by aligning Freebase relations
  with the New York Times corpus. . They used the Stanford named
  entity recognizer \cite{FinkelGrMa05} to find entity mentions in
  text and constructed relation mentions only between entity mentions
  in the same sentence. They also provide a train-test split.

  For evaluation, we follow the same strategy as \cite{RYMM13}.  We
  consider each relation as a query and receive the top $1000$ (run
  depth) tuples from each system. While outputting the ranked list, we
  do not output two tuples differing only in the evidence. If two such
  tuples belong to the top $1000$, we keep the one with the higher
  score. Next we pool the top $50$ (pool depth) answers from each
  system and manually judge their correctness. This gives a set of
  relevant results that we use to calculate an average precision
  across the precision-recall curve, and an aggregate mean average
  precision (MAP) across all relations. We also report weighted MAP,
  where the average precision of each relation is weighed by the total
  number of true facts for that relation.

  Table \ref{tab:map} compares the performance of our pattern methods
  with the learning based methods of \cite{MBSJ09}(Mintz) and
  \cite{STNM12}(Miml). For the pattern based approaches, we compute the
  $\mathrm{Accuracy}(\cdot)$ values from the training data, and use
  these values to score the tuples from the test. For the learning
  based methods, we used the same feature sets that were used in the
  previous works. The implementation in the code base of \cite{STNM12}
  was used for the learning based methods. 

  We see that Mintz and Miml perform at par in the task of WRE, and
  surprisingly, SimplePat outperforms both these methods. This shows
  that the pattern based methods are more robust than classifiers when
  trained on noisy distantly supervised data. Finally, DecompPat and
  SoftPatMatch both outperform SimplePat, due to the more relaxed
  matching of patterns.

\section{Conclusion}

  In this paper, we develop methods to reliably extract facts from
  text corpora. We introduce a task called Witnessed Relation
  Extraction, which entails extracting facts paired with evidence or
  witness. The coupling of evidence with the fact makes it easy for
  human annotators to verify the correctness of the extracted fact and
  prune erroneous extractions. In this work, we only worked with a
  single sentence as witness. Our future work will aim at lifting this
  restriction, allowing a witness to be multiple sentences, or
  combination of multiple knowledge base facts.



%\section*{Acknowledgments}
%Do not number the acknowledgment section.

\bibliography{ccg,cited,patterns}
\bibliographystyle{naaclhlt2016}


\end{document}




%  \newline
%  \newline
%  \noindent
%  \textbf{REE as a better evaluation for relation extractors}
%  Traditionally, relation extractors are evaluated on the veracity of
%  the relational facts $(e_1, r, e_2)$ extracted, irrespective of the
%  reason for the extraction. However, the text that the exractor sees,
%  might not express the relation $r$ between entities $e_1$ and $e_2$,
%  even when $(e_1, r, e_2)$ is true. In such cases, the traditional
%  evaluation will erroneously reward the system for extracting $(e_1,
%  r, e_2)$. In contrast, the output of REE comprises relational facts
%  coupled with evidence found in text for the extraction, and the
%  evaluation is based on whether the evidence supports the extraction,
%  thus removing the aforementioned drawback of traditional evaluation.



  \begin{table*}[!ht]
        \centering \footnotesize
        \begin{tabular}{|l|c|c|c|c|}
          \hline
           Relation & Train Pos-Neg & Test Pos-Neg & Pattern MAP & 
           Classifier MAP\\\hline
          \hline
           place\_of\_birth & 1:10 & 1:10 & 91.6 & 62.3 \\
           place\_of\_birth & 1:10 & 1:500 & 73.3 & 22.6 \\
           company & 1:10 & 1:10 & 78.7 & 79.9 \\
           company & 1:10 & 1:500 & 86.8 & 22.1 \\
           children & 1:10 & 1:10 & 69.5 & 44.6 \\
           children & 1:10 & 1:500 & 94.8 & 3.0 \\
          \hline
        \end{tabular}
        \caption{\footnotesize Comparison of Pattern vs Classifier with
        different positive-negative examples ratio}
        \label{tab:posneg}
  \end{table*}


  \textbf{Why are classifiers doing worse ? } We explore two
  possibilities : First, the patterns of DecompPat are indeed better
  indicators of the relation, which would imply classifier with these
  as features should do well.  Second, the ratio of positive and
  negative ratio is very skewed (for all relations it is worse than
  1:500), which might make it difficult for the classifier to
  function. In order to analyze this, for each relation, we create a
  train set and two test sets, each having atmost $1000$ positive
  examples. The ratio of positive to negative examples is kept 1:10 in
  both train sets, whereas for the test, one has 1:10, and the other
  1:500. Finally, we compared the performance of DecompPat and the
  classifier with the different test sets. Table \ref{tab:posneg}
  shows the comparison.

  The experiments show that the classifier performance is much better
  when the proportion of negative examples is the same in train and
  test. However, when we make the classifier extract relations from
  data with worse positive-negative ratio (1:500) compared to train,
  it performs much worse than the pattern method. Since the real
  proportion of positive-negative examples in the test data is lower
  than 1:500, this explains why classifiers perform poorly on the
  entire test data. These experiments also show that pattern method is
  much more robust than classifiers when the test data has a very
  skewed proportion of postive to negative examples.
